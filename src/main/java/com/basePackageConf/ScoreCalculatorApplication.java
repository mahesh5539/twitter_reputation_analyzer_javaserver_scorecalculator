package com.basePackageConf;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
@Configuration
@ComponentScan(basePackages = { "com.basePackageConf","com.twitter.reputation.analysis","filter"})
@EnableAspectJAutoProxy
@EnableAutoConfiguration
@SpringBootApplication
public class ScoreCalculatorApplication {
	 public static void main(String[] args) {
	        SpringApplication.run(ScoreCalculatorApplication.class, args);
	    }
}
