package com.twitter.reputation.analysis;

public class Tweets {
	private String tweet;
	private int retweet_count;
	private String date;
	private String media;
	private String media_image_url;
	private String[] __proto__ = new String[12]; 
	public String[] get__proto__() {
		return __proto__;
	}

	public void set__proto__(String[] __proto__) {
		this.__proto__ = __proto__;
	}

	public String getMedia_image_url() {
		return media_image_url;
	}

	public void setMedia_image_url(String media_image_url) {
		this.media_image_url = media_image_url;
	}

	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}

	public int getRetweet_count() {
		return retweet_count;
	}

	public void setRetweet_count(int retweet_count) {
		this.retweet_count = retweet_count;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTweet() {
		return tweet;
	}

	public void setTweet(String tweet) {
		this.tweet = tweet;
	}

}
