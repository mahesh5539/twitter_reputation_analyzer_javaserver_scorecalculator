package com.twitter.reputation.analysis;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;



@Controller
public class ReputationScoreCalculate {
	JSONObject response = new JSONObject();
	@RequestMapping(value = "/getReputation/{follower}", method = RequestMethod.POST,consumes = {"application/json;charset=UTF-8"}, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<String> CalculateScoreProfile(@RequestBody String tweets,@PathVariable int follower ) throws JSONException, IOException {		
		ReadTheWordFiles readTheWordFiles = new ReadTheWordFiles();
		readTheWordFiles.ReadFiles();		
		String result = tweets.replaceAll("[-+.^:,\"#$@!]"," ");		
		List<String> tokens = Arrays.asList(result.split("\\s+"));
		int totalWords = tokens.size();
		
		Iterator<String> wordListIterator = tokens.iterator();
		while (wordListIterator.hasNext()) {
			String tempListVal = wordListIterator.next();
			if(readTheWordFiles.positiveWordHashMap.containsKey(tempListVal)){
				int tempCountValPos = readTheWordFiles.positiveWordHashMap.get(tempListVal);
				tempCountValPos++;
				readTheWordFiles.positiveWordHashMap.put(tempListVal,tempCountValPos );
			}
			else if(readTheWordFiles.negativeWordHashMap.containsKey(tempListVal)){
				int tempCountValNeg = readTheWordFiles.negativeWordHashMap.get(tempListVal);
				tempCountValNeg++;
				readTheWordFiles.negativeWordHashMap.put(tempListVal,tempCountValNeg );
			}
		}
		int finalPositiveCount= 0;
		for(String key:readTheWordFiles.positiveWordHashMap.keySet()){
			finalPositiveCount = finalPositiveCount + readTheWordFiles.positiveWordHashMap.get(key);
		}
		int finalNegativeCount= 0;
		for(String key:readTheWordFiles.negativeWordHashMap.keySet()){
			finalNegativeCount = finalNegativeCount + readTheWordFiles.negativeWordHashMap.get(key); 
		}

		//check what if a user has no tweets
		//var x = divide number of followers by 100,0000
		// then consider x's value out of 100 and calculate a new ratio for 333. This final base score
		// negative scale (number of -ve words * 100 * x)/total number of words
		// positive scale (number of +ve words*100*x)/total number of words		
		// final score = base score+ positive scale - negative scale
		// return the score, positive, negative number of words.
		
		if(totalWords==0 || follower ==0 ){
			response.put("Reputation", "null");
			return new ResponseEntity<String>(response.toString(),HttpStatus.NO_CONTENT); 
		}
		if(follower>1000000){
		float scaleOnFollowers = follower/1000000;
		float baseScore = (333*scaleOnFollowers)/ 100;
		float negativeScale = (finalNegativeCount * 100 * scaleOnFollowers)/totalWords;
		float positiveScale = (finalPositiveCount * 100 * scaleOnFollowers)/totalWords;
		float finalScore = baseScore + positiveScale -negativeScale;
		int finalIntScore = Math.round(finalScore);
		if(finalIntScore > 1000){
			finalIntScore = 1000;
		}
			try {
				response.put("finalScore",finalIntScore );
				response.put("negativeScore",finalNegativeCount );
				response.put("positiveScore",finalPositiveCount );
			} catch (JSONException e) {
				e.printStackTrace();
			} 
			
			 return new ResponseEntity<String>(response.toString(),HttpStatus.OK);
		}
		if(follower<=1000000){
			float scaleOnFollowersSm = follower/100000;
			float baseScoreSm = (333*scaleOnFollowersSm)/ 100;
			float negativeScaleSm = (finalNegativeCount * 100 * scaleOnFollowersSm)/totalWords;
			float positiveScaleSm = (finalPositiveCount * 100 * scaleOnFollowersSm)/totalWords;
			float finalScoreSm = baseScoreSm + positiveScaleSm -negativeScaleSm;
			int finalIntScore = Math.round(finalScoreSm);
			if(finalIntScore > 1000){
				finalIntScore = 1000;
			}
				try {
					response.put("finalScore",finalIntScore );
					response.put("negativeScore",finalNegativeCount );
					response.put("positiveScore",finalPositiveCount );
				} catch (JSONException e) {
					e.printStackTrace();
				} 
				
				 return new ResponseEntity<String>(response.toString(),HttpStatus.OK);
			}
		return new ResponseEntity<String>(response.toString(),HttpStatus.NO_CONTENT); 	
	}
	
}
