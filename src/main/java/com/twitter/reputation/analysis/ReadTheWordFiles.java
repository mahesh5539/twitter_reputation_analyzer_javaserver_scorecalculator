package com.twitter.reputation.analysis;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import org.springframework.core.io.FileSystemResource;

public class ReadTheWordFiles {
	String positiveWordFile = "C:\\Users\\Adonis_M\\workspace\\ScoreCalculator\\src\\main\\java\\positive_word_list.text";
	String negativeWordFile = "C:\\Users\\Adonis_M\\workspace\\ScoreCalculator\\src\\main\\java\\negative_word_list.text";
    HashMap<String, Integer> positiveWordHashMap = new HashMap<String, Integer>();
    HashMap<String, Integer> negativeWordHashMap = new HashMap<String, Integer>();
    String positiveFileLine;
    String negativeFileLine;
    int positiveValue = 0;
    int negativeValue = 0;
    
	public void ReadFiles() throws IOException{
		
	    
	    BufferedReader readerPositiveFile = new BufferedReader(new FileReader(positiveWordFile));
	    while ((positiveFileLine = readerPositiveFile.readLine()) != null)
	    {
	        String[] parts = positiveFileLine.split("/n");	       
	            String key = parts[0];
	            positiveWordHashMap.put(key, positiveValue);
	     }
	    readerPositiveFile.close();
	    
	    BufferedReader readerNegativeFile = new BufferedReader(new FileReader(negativeWordFile));
	    while ((negativeFileLine = readerNegativeFile.readLine()) != null)
	    {
	        String[] parts = negativeFileLine.split("/n");	       
	            String key = parts[0];
	            negativeWordHashMap.put(key, negativeValue);
	     }
	    readerNegativeFile.close();
	 
	}

}
